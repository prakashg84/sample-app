FROM fabric8/java-alpine-openjdk11-jre:latest

ENV JAVA_APP_JAR sample-app.jar
ENV AB_ENABLED off
ENV AB_JOLOKIA_AUTH_OPENSHIFT true
ENV JAVA_OPTIONS -Xmx256m 

EXPOSE 8080

RUN chmod -R 777 /deployments/
ADD target/sample-app.jar /deployments/ 